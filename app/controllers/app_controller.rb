class AppController < ApplicationController
  layout 'app'
  before_action :authenticate_client!
end
