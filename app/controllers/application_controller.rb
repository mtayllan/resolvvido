class ApplicationController < ActionController::Base
  def main
  end

  def order
    @client = Client.new
    @client.orders.build
  end

  def create
    @client = Client.new(client_params)
    respond_to do |format|
      if @client.save!
        format.html { redirect_to pedido_sucesso_path }
      else
        format.html { render :order, notice: @client.errors.full_messages }
      end
    end
  end

  def success
  end

  private

  def client_params
    params.require(:client).permit(:name, :email, :phone, :city, :state, :password,
      orders_attributes: [:problem_type, :description, :date])
  end
end
