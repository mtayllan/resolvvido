class Adv::MatchesController < AdvController
  def index
    @matches = current_lawyer.matches.awaiting_approval
  end

  def update
    @match = Match.find(params[:id])
    respond_to do |format|
      if @match.update(situation: params[:situation])
        format.html { redirect_to adv_matches_path, notice: 'Match atualizado com sucesso' }
      else
        format.html { render :index, notice: @match.errors.full_messages }
      end
    end
  end
end
