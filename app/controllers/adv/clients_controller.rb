class Adv::ClientsController < AdvController
  def index
    @clients = current_lawyer.clients
  end
end
