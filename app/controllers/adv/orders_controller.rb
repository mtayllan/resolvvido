class Adv::OrdersController < AdvController
  def index
    @orders =
      if params[:client_id] && Client.find(params[:client_id]).lawyer == current_lawyer
        Client.find(params[:client_id]).orders
      else
        current_lawyer.orders.order(updated_at: :desc)
      end
  end

  def show
    @order = Order.find(params[:id])
    @client = @order.client
  end

  def update
    @order = Order.find(params[:id])
    respond_to do |format|
      if @order.update(status: params[:status])
        format.html { redirect_to adv_orders_path, notice: 'Pedido atualizado com sucesso' }
      else
        format.html { render :index, notice: @order.errors.full_messages }
      end
    end
  end
end
