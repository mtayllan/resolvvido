class App::OrdersController < AppController
  before_action :check_lawyer, only: [:new, :create]

  def index
    @orders = current_client.orders.order(updated_at: :desc)
  end

  def show
    @order = Order.find(params[:id])
    @lawyer = @order.lawyer
  end

  def new
    @order = Order.new
  end

  def create
    @order = Order.new(order_params)
    @order.client = current_client
    respond_to do |format|
      if @order.save!
        format.html { redirect_to app_orders_path, notice: 'Pedido feito com sucesso' }
      else
        format.html { render :new, notice: @order.errors.full_messages }
      end
    end
  end

  private

  def check_lawyer
    redirect_to app_orders_path if current_client.lawyer.nil?
  end

  def order_params
    params.require(:order).permit(:date, :description, :problem_type)
  end
end
