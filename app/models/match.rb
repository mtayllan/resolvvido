class Match < ApplicationRecord
  belongs_to :client
  belongs_to :lawyer

  validates :situation, presence: true

  enum situation: { awaiting_approval: 0, refused: 1, accepted: 2 }

  after_update :connect_client_to_lawyer, if: proc { accepted? }

  private

  def connect_client_to_lawyer
    client.lawyer = lawyer
    client.save!
  end
end
