class Order < ApplicationRecord
  belongs_to :client
  has_one :lawyer, through: :client

  enum problem_type: { flight: 0, baggage: 1 }
  enum status: { pending: 0, under_review: 1, success: 2, failed: 3, canceled: 4 }

  validates :description, :date, presence: true

  after_create :generate_match, if: proc { client.orders.count == 1 }

  private

  def generate_match
    lawyer = Lawyer.where(state: client.state).order(clients_count: :asc).first
    lawyer = Lawyer.order(clients_count: :asc).first if lawyer.nil?
    Match.create(
      client: client,
      lawyer: lawyer,
      situation: 'awaiting_approval'
    )
  end
end
