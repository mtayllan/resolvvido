class Client < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  belongs_to :lawyer, optional: true, counter_cache: true
  has_many :orders, dependent: :restrict_with_error
  has_many :matches, dependent: :restrict_with_error
  accepts_nested_attributes_for :orders

  validates :name, :city, :state, :name, :phone, presence: true
end
