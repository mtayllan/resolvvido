class Lawyer < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  has_many :clients, dependent: :restrict_with_error
  has_many :orders, through: :clients
  has_many :matches, dependent: :restrict_with_error

  validates :cpf, :city, :state, :phone, :name, presence: true
end
