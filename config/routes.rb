Rails.application.routes.draw do
  # devise routes
  devise_for :lawyers
  devise_for :clients, controllers: { sessions: 'clients/sessions' }
  as :client do
    get 'login', to: 'clients/sessions#new'
  end

  # site routes
  root to: 'application#main'
  get 'pedido', to: 'application#order'
  post 'order', to: 'application#create'
  get 'pedido/sucesso', to: 'application#success'

  # app routes
  namespace :app do
    root to: 'orders#index'
    resources :orders, only: [:index, :show, :new, :create], path: 'pedidos'
    get 'meu_advogado', to: 'lawyers#show', as: :lawyer
  end

  namespace :adv do
    root to: 'orders#index'
    resources :orders, only: [:index, :show, :update], path: 'pedidos'
    resources :clients, only: [:index, :show], path: '/clients' do
      resources :orders, only: [:index], path: 'pedidos'
    end
    resources :matches, only: [:index, :update]
  end
end
