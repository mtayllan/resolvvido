require 'rails_helper'

RSpec.describe Client, type: :model do
  it { is_expected.to belong_to(:lawyer).optional }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:city) }
  it { is_expected.to validate_presence_of(:state) }
  it { is_expected.to validate_presence_of(:phone) }
end
