require 'rails_helper'

RSpec.describe Order, type: :model do
  it { is_expected.to belong_to(:client) }
  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:date) }
  it { is_expected.to define_enum_for(:problem_type).with_values([:flight, :baggage]) }
  it { is_expected.to define_enum_for(:status).with_values([:pending, :under_review, :success, :failed, :canceled]) }

  describe 'when is the first order of his client' do
    before do
      create(:lawyer)
    end

    let(:order) { create(:order, status: :pending) }

    it 'creates a match' do
      expect(order.client.matches.count).to eq(1)
    end
  end
end
