require 'rails_helper'

RSpec.describe Match, type: :model do
  it { is_expected.to belong_to(:client) }
  it { is_expected.to belong_to(:lawyer) }
  it { is_expected.to define_enum_for(:situation).with_values([:awaiting_approval, :refused, :accepted]) }
  it { is_expected.to validate_presence_of(:situation) }

  context 'when a lawyer is from the same state of the client' do
    let(:state) { Faker::Address.state }

    before do
      3.times do
        create(:lawyer, state: state)
        create_list(:client, 3, lawyer: Lawyer.last, state: state)
      end
    end

    context 'with 0 clients and the other lawyers have clients' do
      let(:client) { create(:client, state: state) }
      let!(:lawyer) { create(:lawyer, state: state) }
      let(:order) { create(:order, client: client) }

      it 'connects the client to this lawyer' do
        expect(order.client.matches.first.lawyer).to eq(lawyer)
      end
    end
  end

  context 'when changes to accepted' do
    let(:match) { create(:match, situation: 'awaiting_approval') }

    context 'when is before acceptation' do
      it { expect(match.client.lawyer).to be_nil }
    end

    context 'when is already accepted' do
      before do
        match.accepted!
      end

      it { expect(match.client.lawyer).to eq(match.lawyer) }
    end
  end
end
