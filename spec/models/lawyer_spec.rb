require 'rails_helper'

RSpec.describe Lawyer, type: :model do
  it { is_expected.to have_many(:clients) }
  it { is_expected.to have_many(:orders).through(:clients) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:city) }
  it { is_expected.to validate_presence_of(:state) }
  it { is_expected.to validate_presence_of(:phone) }
  it { is_expected.to validate_presence_of(:cpf) }
end
