FactoryBot.define do
  factory :client do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    phone { Faker::PhoneNumber.cell_phone }
    city { Faker::Address.city }
    state { Faker::Address.state }
  end
end
