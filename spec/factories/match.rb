FactoryBot.define do
  factory :match do
    lawyer
    client
    situation { Match.situations.keys.sample }
  end
end
