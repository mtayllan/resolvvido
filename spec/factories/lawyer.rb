FactoryBot.define do
  factory :lawyer do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
    phone { Faker::PhoneNumber.cell_phone }
    city { Faker::Address.city }
    state { Faker::Address.state }
    cpf { CPF.generate(true) }
  end
end
