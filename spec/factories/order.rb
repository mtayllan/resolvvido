FactoryBot.define do
  factory :order do
    client
    problem_type { Order.problem_types.keys.sample }
    status { Order.statuses.keys.sample }
    description { Faker::Lorem.paragraphs }
    date { [Time.zone.today, Time.zone.today - [1, 2, 3].sample.month].sample }
  end
end
