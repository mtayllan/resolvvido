Lawyer.create(
  email: 'l@l.com',
  password: '123456',
  name: Faker::Name.name,
  phone: Faker::PhoneNumber.cell_phone,
  state: 'Ceará',
  city: 'Fortaleza',
  cpf: CPF.generate(true)
)

Client.create(
  email: 'u@u.com',
  password: '123456',
  name: Faker::Name.name,
  phone: Faker::PhoneNumber.cell_phone,
  state: 'Ceará',
  city: 'Fortaleza'
)

# create orders
Order.create(
  client_id: 1,
  problem_type: [0, 1].sample,
  status: [0, 1, 2, 3, 4].sample,
  description: Faker::Lorem.paragraphs,
  date: [Time.zone.today, Time.zone.today - 1.month].sample
)

Match.first.update(situation: 'accepted')

5.times do
  Lawyer.create(
    email: Faker::Internet.email,
    password: '123456',
    name: Faker::Name.name,
    phone: Faker::PhoneNumber.cell_phone,
    state: Faker::Address.state,
    city: Faker::Address.city,
    cpf: CPF.generate(true)
  )
end

5.times do
  Client.create(
    email: Faker::Internet.email,
    password: '123456',
    name: Faker::Name.name,
    phone: Faker::PhoneNumber.cell_phone,
    state: 'Ceará',
    city: 'Fortaleza',
    lawyer_id: [2, 3].sample
  )
end

10.times do
  Order.create(
    client_id: [1, 2, 3].sample,
    problem_type: [0, 1].sample,
    status: [0, 1, 2, 3, 4].sample,
    description: Faker::Lorem.paragraphs,
    date: [Time.zone.today, Time.zone.today - 1.month].sample
  )
end
