class CreateMatches < ActiveRecord::Migration[5.2]
  def change
    create_table :matches do |t|
      t.references :client, foreign_key: true
      t.references :lawyer, foreign_key: true
      t.integer :situation

      t.timestamps
    end
  end
end
