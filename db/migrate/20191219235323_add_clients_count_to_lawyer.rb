class AddClientsCountToLawyer < ActiveRecord::Migration[5.2]
  def change
    add_column :lawyers, :clients_count, :integer, default: 0
  end
end
