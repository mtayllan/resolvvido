class AddLawyerReferenceToClient < ActiveRecord::Migration[5.2]
  def change
    add_reference :clients, :lawyer, foreign_key: true
  end
end
