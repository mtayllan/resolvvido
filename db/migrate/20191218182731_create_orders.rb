class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :problem_type
      t.integer :status, default: 0
      t.text :description
      t.date :date

      t.references :client, foreign_key: true

      t.timestamps
    end
  end
end
