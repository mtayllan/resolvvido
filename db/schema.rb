# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_19_235323) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name"
    t.string "phone"
    t.string "state"
    t.string "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "lawyer_id"
    t.index ["email"], name: "index_clients_on_email", unique: true
    t.index ["lawyer_id"], name: "index_clients_on_lawyer_id"
    t.index ["reset_password_token"], name: "index_clients_on_reset_password_token", unique: true
  end

  create_table "lawyers", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name"
    t.string "phone"
    t.string "state"
    t.string "city"
    t.string "cpf"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "clients_count", default: 0
    t.index ["email"], name: "index_lawyers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_lawyers_on_reset_password_token", unique: true
  end

  create_table "matches", force: :cascade do |t|
    t.bigint "client_id"
    t.bigint "lawyer_id"
    t.integer "situation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_matches_on_client_id"
    t.index ["lawyer_id"], name: "index_matches_on_lawyer_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "problem_type"
    t.integer "status", default: 0
    t.text "description"
    t.date "date"
    t.bigint "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_id"], name: "index_orders_on_client_id"
  end

  add_foreign_key "clients", "lawyers"
  add_foreign_key "matches", "clients"
  add_foreign_key "matches", "lawyers"
  add_foreign_key "orders", "clients"
end
