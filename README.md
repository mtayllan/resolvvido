# Resolvvi Coding Challenge
App **Resolvvido**

Client padrão: u@u.com -> 123456

Advogado padrão: l@l.com -> 123456

[Repositório](https://gitlab.com/mtayllan/resolvvido)

[Heroku](http://resolvvido.herokuapp.com/)

- Como você abordou o desafio?
> Primeiro analisei tudo que foi dito no pdf e então já parti para fazer um pequeno flowchart de como funcionaria a aplicação. Feito isso já desenvolvi um pequeno modelo de banco de dados para detalhar melhor informações e relações que seriam necessárias. Feito isso fiz um passo a passo detalhado de cada ação que iria realizar antes de realmente partir para o código.
> Durante o desenvolvimento enfrentei alguns imprevistos externos, mas acredito ter conseguido desenvolver uma boa versão inicial da aplicação.
> Infelizmente devido os imprevistos tive que abandonar alguns testes automáticos necessários pra assegurar as funcionalidades.

- Qual schema design você escolheu e por que?
> ![](https://i.imgur.com/ZyM5Pdt.png)
> Atualmente o sistema funciona com esses 4 models, fiz essa escolha baseada no que foi descrito pelo desafio.
> As páginas que temos estão divididas em 3 áreas.
> - Site(root) (inicial e cadastro de conta + pedido)
> - Usuário(/app) (login, pedidos, pedido único e meu advogado)
> - Advogado(/adv) (login, pedidos, clientes, matches)

- Se você tivesse mais um dia para trabalhar no desafio, como o usaria? E se você tivesse um mês?
> Começaria fazendo mais alguns testes pra asseguras as atuais funcionalidades.
> Depois disso procuraria desenvolver uma melhor identidade visual pro sistema, está apenas um esqueleto basicamente.
> Com essas duas atividades prontas definiria um quadro de atividades e organizaria melhor as prioridades a serem executadas com base nos feedbacks dos possíveis usuários que já estariam utilizando o sistema. Como primeiras atividade já colocaria a definição de uma fila de e-mails e melhorias no algoritmo de match.
